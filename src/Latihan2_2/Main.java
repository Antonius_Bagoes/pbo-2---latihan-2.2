/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_2;

/**
 *
 * @author anton
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Kantor kantor = new Kantor();
        Pegawai[] pegawai = new Pegawai[10];
        /*
        Manager = (int jumAnak, int jamKerja, int istri, String tugasBelajar, String nip, String nama, int golongan)
        Marketing = (int jamKerja, String tugasBelajar, String nip, String nama, int golongan)
        Honorer = (String nip, String nama, int golongan)
         */
        pegawai[0] = new Manager(2, 10, 1, "Studi", "P.1234", "Bagus", 3);
        pegawai[1] = new Marketing(9, "Studi", "P.2345", "Hendi", 3);
        pegawai[2] = new Marketing(8, "Selesai", "P.6789", "Saputro", 3);
        pegawai[3] = new Marketing(7, "Studi", "P.1011", "Anton", 2);
        pegawai[4] = new Marketing(10, "Selesai", "P.1213", "Nius", 1);
        pegawai[5] = new Honorer("P.8787", "Heru", 1);
        pegawai[6] = new Honorer("P.8989", "Pius", 2);
        pegawai[7] = new Honorer("P.6223", "Tejo", 3);
        pegawai[8] = new Honorer("P.7845", "Bejo", 2);
        pegawai[9] = new Honorer("P.1987", "Untung", 1);
        kantor.setManager((Manager) pegawai[0]);
        Pegawai pegawaiTemp[] = new Pegawai[pegawai.length - 1];
        int count = 0;
        for (int i = 0; i < pegawai.length; i++) {
            if ((pegawai[i] instanceof Marketing || pegawai[i] instanceof Honorer)) {
                pegawaiTemp[count] = pegawai[i];
                count++;
            }
        }
        kantor.setPegawai(pegawaiTemp);
        pegawaiTemp = null;
        System.out.println("Nama Manager    : " + kantor.getManager().getNama());
        System.out.println("Daftar Pegawai");
        System.out.println("=========================================================================================");
        System.out.printf("%-4s", "No.");
        System.out.printf("%-10s", "NIP");
        System.out.printf("%-15s", "Nama");
        System.out.printf("%-10s", "Studi");
        System.out.printf("%-20s", "Gaji");
        System.out.println("\n=========================================================================================");
        for (int i = 0; i < kantor.getPegawai().length; i++) {
            System.out.printf("%-4s", (i + 1));
            System.out.printf("%-10s", kantor.getPegawai()[i].getNip());
            System.out.printf("%-15s", kantor.getPegawai()[i].getNama());
            if (kantor.getPegawai()[i] instanceof Marketing) {
                if (((Marketing) kantor.getPegawai()[i]).isSelesai() == true) {
                    System.out.printf("%-10s", "Studi");
                } else {
                    System.out.printf("%-10s", "-");
                }
                System.out.printf("%-20s", "Rp. " + kantor.getPegawai()[i].hitungGatot());
            } else if (kantor.getPegawai()[i] instanceof Honorer) {
                System.out.printf("%-10s", "-");
                System.out.printf("%-20s", "Rp. " + kantor.getPegawai()[i].hitungGatot());
            }
            System.out.println("");

        }
        System.out.println("\n=========================================================================================");
        System.out.println("Jumlah Gaji Kantor : Rp. " + kantor.hitGajiPeg());

    }

}
