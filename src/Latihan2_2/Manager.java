/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_2;

/**
 *
 * @author anton
 */
public class Manager extends Pegawai implements TugasBelajar {

    private int jumAnak;
    private int jamKerja;
    private int istri;
    private boolean tugasBelajar;

    public Manager(int jumAnak, int jamKerja, int istri, String tugasBelajar, String nip, String nama, int golongan) {
        super(nip, nama, golongan);
        this.jumAnak = jumAnak;
        this.jamKerja = jamKerja;
        this.istri = istri;
        tugasBelajar = tugasBelajar.toLowerCase();
        if ("studi".equals(tugasBelajar)) {
            this.tugasBelajar = true;
        } else if ("selesai".equals(tugasBelajar)) {
            this.tugasBelajar = false;
        }

    }

    public int getJumAnak() {
        return jumAnak;
    }

    public void setJumAnak(int jumAnak) {
        this.jumAnak = jumAnak;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public int getIstri() {
        return istri;
    }

    public void setIstri(int istri) {
        this.istri = istri;
    }

    public boolean isTugasBelajar() {
        return tugasBelajar;
    }

    public void setTugasBelajar(boolean tugasBelajar) {
        this.tugasBelajar = tugasBelajar;
    }

    public double hitTunjangan() {
        if (jumAnak <= 3) {
            return jumAnak * 100000;
        } else {
            return 3 * 100000;
        }
    }

    public double hitLembur() {
        if (jamKerja > 8) {
            return (jamKerja - 8) * 50000;
        } else {
            return 0;
        }
    }

    @Override
    public double hitungGatot() {
        return hitGajiPokok() + hitTunjangan() + hitLembur();
    }

    @Override
    public boolean isSelesai() {
        return tugasBelajar;
    }

}
