/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_2;

/**
 *
 * @author anton
 */
public class Kantor {

    Manager manager;
    Pegawai pegawai[];

    public Kantor() {
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Pegawai[] getPegawai() {
        return pegawai;
    }

    public void setPegawai(Pegawai[] pegawai) {
        this.pegawai = pegawai;
    }

    public double hitGajiPeg() {
        double totalTemp = 0;
        for (int i = 0; i < pegawai.length; i++) {
            totalTemp += pegawai[i].hitungGatot();
        }
        totalTemp += manager.hitungGatot();
        return totalTemp;
    }
}
