/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_2;

/**
 *
 * @author anton
 */
public class Pegawai implements Pendapatan {

    protected String nip;
    protected String nama;
    protected int golongan;
    protected double gajiTotal;

    public Pegawai(String nip, String nama, int golongan) {
        this.nip = nip;
        this.nama = nama;
        this.golongan = golongan;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGolongan() {
        return golongan;
    }

    public void setGolongan(int golongan) {
        this.golongan = golongan;
    }

    public double hitGajiPokok() {
        switch (golongan) {
            case 1:
                return 500000;
            case 2:
                return 750000;
            case 3:
                return 1000000;
            default:
                break;
        }
        return 1;
    }

    @Override
    public double hitungGatot() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
