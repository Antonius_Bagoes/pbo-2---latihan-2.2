/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_2;

/**
 *
 * @author anton
 */
public class Honorer extends Pegawai {

    public Honorer(String nip, String nama, int golongan) {
        super(nip, nama, golongan);
    }

    @Override
    public double hitungGatot() {
        return hitGajiPokok();
    }
}
