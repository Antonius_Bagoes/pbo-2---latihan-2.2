/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_2;

/**
 *
 * @author anton
 */
public class Marketing extends Pegawai implements TugasBelajar {

    private int jamKerja;
    private boolean tugasBelajar;

    public Marketing(int jamKerja, String tugasBelajar, String nip, String nama, int golongan) {
        super(nip, nama, golongan);
        this.jamKerja = jamKerja;
        tugasBelajar = tugasBelajar.toLowerCase();
        if ("studi".equals(tugasBelajar)) {
            this.tugasBelajar = true;
        } else if ("selesai".equals(tugasBelajar)) {
            this.tugasBelajar = false;
        }
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public double hitLembur() {
        if (jamKerja > 8) {
            return (jamKerja - 8) * 50000;
        } else {
            return 0;
        }
    }

    @Override
    public double hitungGatot() {
        return hitGajiPokok() + hitLembur();
    }

    @Override
    public boolean isSelesai() {
        return tugasBelajar;
    }

}
